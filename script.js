// ------------------------------------ soal 1 ------------------------------------//
// normal javascript
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }   

golden()

//es 6
const goldenes6 = () => console.log("this is golden!!")
    
goldenes6()

// ------------------------------------ soal 2 ------------------------------------//
// normal javascript
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }

  newFunction("William", "Imoh").fullName()

//es 6
const newFunctiones6 = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: () => console.log(firstName + " " + lastName)
  }
}

newFunctiones6("William", "Imoh").fullName()

// ------------------------------------ soal 3 ------------------------------------//
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const { firstName, lastName, destination, occupation, spell } = newObject;

  console.log(firstName, lastName, destination, occupation)

// ------------------------------------ soal 4 ------------------------------------//
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

const combined = [...west, ...east]
console.log(combined)

// ------------------------------------ soal 5 ------------------------------------//
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet 
consectetur adipiscing elit ${planet} do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim
ad minim veniam`  
console.log(before) 